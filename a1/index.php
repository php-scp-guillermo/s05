<?php
session_start();
if (isset($_SESSION['user'])) {
    header('Location: ./home.php');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        * {
            padding: 0;
            margin: 0;
            box-sizing: border-box;
        }

        form {
            width: 400px;
            padding: 1rem;
            margin: 3rem auto;
            border: 1px solid grey;
        }

        input,
        button {
            width: 100%;
            height: 2rem;
            padding: 0 .5rem;
            margin-bottom: 1rem;
        }
    </style>
</head>

<body>
    <form action="./server.php" method="POST">
        <label for="username">Usename:</label>
        <input type="text" name="username" placeholder="Enter Usename" required />

        <label for="password">Password:</label>
        <input type="password" name="password" placeholder="Enter Password" required />
        <input type="hidden" name="login" value="login">
        <button type="submit"> login</button>
    </form>

</body>

</html>