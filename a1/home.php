<?php
session_start();
if (!isset($_SESSION['user'])) {
    header('Location: ./index.php');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        div,
        h1 {
            text-align: center;
        }
    </style>
</head>

<body>
    <h1>Hello, <?= $_SESSION['user']; ?></h1>
    <form action="./server.php" method="POST">
        <input type="hidden" name="logout" value="logout">
        <div><button type="submit">Log out</button> </div>
    </form>
</body>

</html>